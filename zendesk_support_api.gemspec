lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'zendesk_support_api/version'

Gem::Specification.new do |s|
  s.name = 'zendesk_support_api'
  s.version = ZendeskSupportAPI::VERSION
  s.date = '2020-04-28'
  s.summary = 'Zendesk Support API wrapper gem'
  s.description = 'TBD'
  s.authors = ['Jason Colyer']
  s.email = 'jcolyer2007@gmail.com'
  s.homepage = 'https://gitlab.com/reyloc/zendesk_support_api'
  s.files = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r(^(test|spec|features)/)) }
  end
  # s.homepage = 'TBD'
  s.license = 'MIT'
  s.add_dependency 'faraday', '~>1.0.0'
  s.add_dependency 'oj', '~>3.10.6'
end
