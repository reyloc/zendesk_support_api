# ZendeskSupportAPI


## Documentation

Documentation (generated via yard) can be found [here](https://reyloc.gitlab.io/zendesk_support_api/)

## Installation

The gem can be installed either via rubygems or bundler:

### Rubygems

```bash
gem install zendesk_support_api
```

### Bundler

```ruby
gem "zendesk_support_api"
```

The current requirements for the gem are:

* 'faraday', '~>1.0.0'
* 'oj', '~>3.10.6'

## Configuration

Configuration is done through the creation of a `ZendeskSupportAPI::Client`
instance:

```ruby
require 'zendesk_support_api'

client = ZendeskSupportAPI::Client.new('user@example.com', '123abc', 'https://zendesk.com/api/v2')
```

## Usage

Soon to come

## Changelog

* v0.1.0 - Initial creation of gem
* v0.2.0 - Added Search functions, Fixed some yard syntax/formatting, Changed version number
* v0.3.0 - Changed version number, Added Group functions, Added User.groups function
* v0.4.0 - Added Organization functions, Changed version number
* v0.5.0 - Added DeletedTickets functions, Added Tickets functions, Yard synx fixes, updated version number

## Upcoming changes

* v0.6.0 - Satisfaction
* v0.7.0 - Attachments?
* v0.8.0 - Requests
* v0.9.0 - Sessions
* v0.10.0 - Tags
* v0.11.0 - Views
* v0.12.0 - Triggers
* v0.13.0 - Automations
* v0.14.0 - SLA
* v0.15.0 - Targets
* v0.16.0 - Macros
* v0.17.0 - Brands
* v0.18.0 - Locales
* v0.19.0 - Schedules
* v0.20.0 - Ticket Forms
* v0.21.0 - Ticket Fields
* v0.22.0 - User Fields
* v0.23.0 - Organization Fields
* v0.24.0 - Account Settings
* v1.0.0 - Usage details, any polishing needed

