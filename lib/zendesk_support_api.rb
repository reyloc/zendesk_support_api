# frozen_string_literal: true

require 'faraday'
require 'oj'

# ZendeskSupportAPI - https://developer.zendesk.com/rest_api/docs/support/introduction
module ZendeskSupportAPI
  require 'zendesk_support_api/client'
  require 'zendesk_support_api/version'
end
