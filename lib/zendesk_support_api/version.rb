# frozen_string_literal: true

module ZendeskSupportAPI
  # Outputs the gem version

  VERSION = '0.5.0'
end
