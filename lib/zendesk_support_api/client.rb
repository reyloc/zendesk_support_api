# frozen_string_literal: true

module ZendeskSupportAPI
  # Client class - https://developer.zendesk.com/rest_api/docs/support/introduction#security-and-authentication
  class Client
    require 'zendesk_support_api/deleted_tickets'
    require 'zendesk_support_api/groups'
    require 'zendesk_support_api/jobs'
    require 'zendesk_support_api/organizations'
    require 'zendesk_support_api/search'
    require 'zendesk_support_api/tickets'
    require 'zendesk_support_api/users'

    # Create a new instance of Client
    #
    # @param user [String] - The API username to use
    # @param token [String] - The API token to use
    # @param url [String] - The API URL to use
    #
    # @example
    #   ZendeskSupportAPI::Client.new('user', '123', 'zendesk.com/api')
    #   #=> #<ZendeskSupportAPI::Client:0x00007f88779cb330 @user="user",
    #   #=>    @token="123", @url="zendesk.com/api">
    def initialize(user, token, url)
      @user = user
      @token = token
      @url = url
    end

    # Make a request to the Zendesk Support API
    #
    # @param http_method [Symbol] The HTTP method to utilize
    # @param endpoint [String] The endpoint to hit
    # @param params [Hash] Parameters for the request
    # @return [Hash]
    #
    # @example
    #   client = ZendeskSupportAPI::Client.new('user', '123', 'zendesk.com/api')
    #   client.response(:get, 'users.json')
    #   #=> {users:[{user1},{user2}...{user100}]}

    def request(http_method, endpoint, params = {})
      response = client.public_send(http_method, endpoint, params)
      Oj.load(response.body)
    end

    # Outputs a spinner symbol
    #
    # @param string [String] The string to output at the beginning
    # @param num [Integer] The index of the iteration
    # @return [String]
    #
    # @example
    #   ZendeskSupportAPI::Client.spinner('users', 1) #=> Grabbing users... \
    #   ZendeskSupportAPI::Client.spinner('groups', 3) #=> /

    def spinner(string, num)
      print "Grabbing #{string}... " if num.to_i == 1
      symbols = ['-', '\\', '|', '/']
      print symbols[num.to_i % 4]
      print "\b"
    end

    # Handles responses that create jobs
    #
    # @param job [Hash] - The output from a request that created a job
    # @return [Hash]

    def handle_job(job)
      print 'Checking job'
      while job['job_status']['status'] != 'completed'
        print '.'
        job = ZendeskSupportAPI::Jobs.show(self, job['job_status']['id'])
      end
      puts 'completed'
      job['job_status']['results']
    end

    private

    # Creates a new Faraday instance
    #
    # @return [ZendeskSupportAPI::Client]

    def client
      @client ||= Faraday.new(@url) do |c|
        c.request :url_encoded
        c.adapter Faraday.default_adapter
        c.basic_auth "#{@user}/token", @token
      end
    end
  end
end
