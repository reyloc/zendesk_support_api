# frozen_string_literal: true

module ZendeskSupportAPI
  # Users class - https://developer.zendesk.com/rest_api/docs/support/users
  class Users
    # Function to return a string that side-loads organizations
    #
    # @return [String]

    def self.included
      'include=organizations'
    end

    # Maps users into user_objects
    #
    # @param users [Array] The Array of users to map
    # @param orgs [Array] The Array of orgs to use in mapping
    # @return [Hash]

    def self.user_map(users, orgs)
      users.map { |u| user_object(u, orgs) }
    end

    # Returns the string of the next_page for pagination
    #
    # @param res [Hash] The Hash containing the response from a request
    # @return [nil|String]
    #
    # @example
    #   ZendeskSupportAPI::Users.next_page(response) #=> nil
    #   ZendeskSupportAPI::Users.next_page(response)
    #   #=> "users.json?include=organizations&page=56

    def self.next_page(res)
      (res['next_page'].nil? ? nil : res['next_page'].split('/').last)
    end

    # Lists out users (first 100)
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @return [Hash]
    #
    # @example
    #   ZendeskSupportAPI::Users.list(client)
    #   #=> {users:[{user1},{user2}...{user100}]}

    def self.list(client)
      res = client.request(:get, "users.json?#{included}")
      res['users'].map { |u| user_object(u, res['organizations']) }
      res['users']
    end

    # Lists out all users (paginates over every page)
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @return [Hash]
    #
    # @example
    #   ZendeskSupportAPI::Users.all(client)
    #   #=> Grabbing users (total: 215336)... / ...done
    #   #=> {users:[{user1},{user2}...{user201520}]}

    def self.all(client)
      users = []
      page = "users.json?#{included}&page=1"
      until page.nil?
        res = client.request(:get, page)
        client.spinner("users (total: #{res['count']})", page.split('=').last)
        users += user_map(res['users'], res['organizations'])
        page = next_page(res)
      end
      puts ' ...done'
      users
    end

    # Grabs a specific user
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param id [Integer] The User's ID to use
    # @return [Hash]
    #
    # @example
    #   ZendeskSupportAPI::Users.show(client, 123)
    #   #=> {
    #   #=>   "id"=>123,
    #   #=>   "url"=>"https://zendesk.com/api/users/123.json",
    #   #=>   "name"=>"Test User",
    #   #=>   ...
    #   #=> }

    def self.show(client, id)
      res = client.request(:get, "users/#{id}.json?#{included}")
      user_object(res['user'], res['organizations'])
    end

    # Shows many users
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param ids [Array] An array of User IDs to use
    # @return [Array]
    #
    # @example
    #   ZendeskSupportAPI::Users.show_many(client, [123, 456])
    #   #=> [
    #   #=>   {
    #   #=>     "id": 123,
    #   #=>     "name": "Johnny Appleseed",
    #   #=>     ...
    #   #=>   },
    #   #=>   {
    #   #=>     "id": 456,
    #   #=>     "name": "Rupert Root",
    #   #=>     ...
    #   #=>   }
    #   #=> ]

    def self.show_many(client, ids)
      ids = ids.join(',')
      res = client.request(:get, "users/show_many.json?#{included}&ids=#{ids}")
      res['users'].map { |u| user_object(u, res['organizations']) }
      res['users']
    end

    # Creates a user hash (for mapping the org into the user Hash)
    #
    # @param user [Hash] The user details to use
    # @param orgs [Array] The Array of orgs to use
    # @return [Hash]

    def self.user_object(user, orgs)
      oid = 'organization_id'
      user['organization'] = orgs.select { |o| o['id'] == user[oid] }
      user
    end

    # Creates a user
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param user [Hash] The user to create
    # @return [Hash|String] Either the user details or the error
    #
    # @example
    #   user = {name: 'Roger Wilco', email: 'rw@example.com'}
    #   ZendeskSupportAPI::Users.create(client, user)
    #   #=> {
    #   #=>   "user": {
    #   #=>     "id":   9873843,
    #   #=>     "name": "Roger Wilco",
    #   #=>     "email": "rw@example.com"
    #   #=>     ...
    #   #=>   }
    #   #=> }
    #   ZendeskSupportAPI::User.create(client, user)
    #   #=> Creation failed: => "Creation failed: {\"email\"=>[{\"description\"
    #   #=> =>\"Email: rw@example.com is already being used by another user\",
    #   #=> \"error\"=>\"DuplicateValue\"}]}"

    def self.create(client, user)
      res = client.request(:post, 'users.json', user: user)
      return "Creation failed: #{res['details']}" if res['error']

      res
    end

    # Creates many users
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param users [Array] The users to create
    # @return [ZendeskSupportAPI::Client.handle_job]

    def self.create_many(client, users)
      res = client.request(:post, 'users/create_many.json', users: users)
      client.handle_job(res)
    end

    # Creates or updates a user
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param user [Hash] The user to create/update
    # @return [Hash|String] Either the user details or the error
    #
    # @example
    #   ZendeskSupportAPI::User.create_or_update(client, user)
    #   #=> {
    #   #=>   "user": {
    #   #=>     "id":   9873843,
    #   #=>     "name": "Roger Wilco",
    #   #=>     "email": "rw@example.com"
    #   #=>     ...
    #   #=>   }
    #   #=> }

    def self.create_or_update(client, user)
      res = client.request(:post, 'users/create_or_update.json', user: user)
      return "Create/Update failed: #{res['description']}" if res['error']

      res
    end

    # Creates or updates many users
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param users [Array] The users to create/update
    # @return [ZendeskSupportAPI::Client.handle_job]

    def self.create_or_update_many(client, users)
      res = client.request(:post,
                           'users/create_or_update_many.json',
                           users: users)
      client.handle_job(res)
    end

    # Updates a user
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param uid [Integer] The User's ID
    # @param user [Hash] The user details to update
    # @return [Hash|String] Either the user details or the error
    #
    # @example
    #   ZendeskSupportAPI::User.update(client, 123, user)
    #   #=> {user}

    def self.update(client, uid, user)
      res = client.request(:put, "users/#{uid}.json", user: user)
      return "Update of #{uid} failed: #{res['error']}" if res['error']

      res
    end

    # Updates many users
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param users [Array] The users to update
    # @return [ZendeskSupportAPI::Client.handle_job]

    def self.update_many(client, users)
      res = client.request(:put, 'users/update_many.json', users: users)
      client.handle_job(res)
    end

    # Deletes a user
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param uid [Integer] The User ID to delete
    # @return [String]
    #
    # @example
    #   ZendeskSupportAPI::Users.delete(client, 123)
    #   #=> User 123 has been deleted
    #   ZendeskSupportAPI::Users.delete(client, 123)
    #   #=> "Deletion of 123 failed: RecordNotFound"

    def self.delete(client, uid)
      res = client.request(:delete, "users/#{uid}.json")
      return "Deletion of #{uid} failed: #{res['error']}" if res['error']

      "User #{uid} has been deleted"
    end

    # Deletes many users
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param ids [Array] The array of User IDs to delete
    # @return [ZendeskSupportAPI::Client.handle_job]

    def self.delete_many(client, ids)
      ids = ids.join(',')
      res = client.request(:delete, "users/destroy_many.json?ids=#{ids}")
      client.handle_job(res)
    end

    # Suspends a user
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param id [Integer] The User ID to suspend
    # @return [String] Either a success message or an error
    #
    # @example
    #   ZendeskSupportAPI::Users.suspend(client, 123)
    #   #=> User 123 is suspended

    def self.suspend(client, id)
      res = client.request(:put, "users/#{id}.json", user: { suspended: true })
      return "Suspension of #{id} failed: #{res['error']}" if res['error']

      "User #{id} suspended" if res['user']['suspended']
    end

    # Shows a users groups
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param uid [Integer] The User ID to use
    # @return [Array]
    #
    # @example
    #   ZendeskSupportAPI::Users.groups(client, 1234)
    #   #=> [
    #   #=>   {
    #   #=>     "name":       "DJs",
    #   #=>     "created_at": "2009-05-13T00:07:08Z",
    #   #=>     "updated_at": "2011-07-22T00:11:12Z",
    #   #=>     "id":         211
    #   #=>   },
    #   #=>   {
    #   #=>     "name":       "MCs",
    #   #=>     "created_at": "2009-08-26T00:07:08Z",
    #   #=>     "updated_at": "2010-05-13T00:07:08Z",
    #   #=>     "id":         122
    #   #=>   }
    #   #=> ]

    def self.groups(client, uid)
      client.request(:get, "users/#{uid}/groups.json")['groups']
    end
  end
end
