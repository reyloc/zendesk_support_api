# frozen_string_literal: true

module ZendeskSupportAPI
  # Tickets class - https://developer.zendesk.com/rest_api/docs/support/tickets
  class Tickets
    # Determines if given string is a valid sort function
    #
    # @param sort [String] The sort string to use
    # @return [Boolean]
    #
    # @example
    #   ZendeskSupportAPI::Tickets.sort_valid? 'assignee' #=> true
    #   ZendeskSupportAPI::Tickets.sort_valid? 'blah' #=> false

    def self.sort_valid?(sort)
      valid = %w[assignee assignee_name created_at group id locale requester
                 requester.name status subject updated_at]
      valid.include?(sort)
    end

    # Prints out the include string
    #
    # @return [String]
    #
    # @example
    #   ZendeskSupportAPI::Tickets.includes
    #   #=> '?include=users,organizations,ticket_forms,comment_count'

    def self.includes
      '?include=users,organizations,ticket_forms,comment_count'
    end

    # Prints out the sort_by and order_by string for the url
    #
    # @param sort [String] The sort string to use
    # @param order [String] The order string to use
    # @return [String]
    #
    # @example
    #   ZendeskSupportAPI::Tickets.sort_order('id', 'desc')
    #   #=> '&sort_by=id&order_by=desc'

    def self.sort_order(sort, order)
      "&sort_by=#{sort}&order_by=#{order}"
    end

    # Maps users into user_objects
    #
    # @param tickets [Array] The Array of tickets to map
    # @param res [Hash] The has containing the response from a request
    # @return [Hash]

    def self.ticket_map(tickets, res)
      tickets.map { |t| ticket_object(t, res) }
    end

    # Returns the string of the next_page for pagination
    #
    # @param res [Hash] The Hash containing the response from a request
    # @return [nil|String]

    def self.next_page(res)
      (res['next_page'].nil? ? nil : res['next_page'].split('/').last)
    end

    # Prints out organization_id
    #
    # @return [String]
    #
    # @example
    #   ZendeskSupportAPI::Tickets.org_id #=> 'organization_id'

    def self.org_id
      'organization_id'
    end

    # Creates a ticket object
    #
    # @param ticket [Hash] The ticket Hash to use
    # @param res [Array] The response to use for mapping
    # @return [Hash]

    def self.ticket_object(ticket, res)
      ticket.merge(
        organization: select_obj(res['organizations'], ticket[org_id]),
        form: select_obj(res['ticket_forms'], ticket['form_id']),
        requester: select_obj(res['users'], ticket['requester_id']),
        assignee: select_obj(res['users'], ticket['assignee_id'])
      )
    end

    # Selects an object from an array based on a user ID
    #
    # @param array [Array] An array of to look in
    # @param id [Integer] The ID to use
    # @return [Hash]

    def self.select_obj(array, id)
      array.select { |a| a['id'] == id }.first
    end

    # List out tickets (first 100)
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param sort [String] The sort method to use
    # @param order [String] The order method to use
    # @return [Array]
    #
    # @example
    #   ZendeskSupportAPI::Tickets.list(client)
    #   #=> [
    #   #=>   {
    #   #=>     "id":      35436,
    #   #=>     "subject": "Help I need somebody!",
    #   #=>     ...
    #   #=>   },
    #   #=>   {
    #   #=>     "id":      20057623,
    #   #=>     "subject": "Not just anybody!",
    #   #=>     ...
    #   #=>   },
    #   #=>   ...
    #   #=> ]

    def self.list(client, sort = 'id', order = 'asc')
      return "Invalid sort '#{sort}'" unless sort_valid?(sort)
      return "Invalid order '#{order}'" unless %w[asc desc].include?(order)

      url = "tickets.json#{includes}#{sort_order(sort, order)}"
      res = client.request(:get, url)
      res['tickets'].map { |t| ticket_object(t, res) }
    end

    # Lists out all tickets (paginates over every page)
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @return [Array]
    #
    # @example
    #   ZendeskSupportAPI::Tickets.all(client)
    #   #=> Grabbing tickets (total: 215336)... / ...done
    #   #=> [{ticket1},{ticket2}...{ticket215336}]

    def self.all(client)
      tickets = []
      page = "tickets.json#{includes}&page=1"
      until page.nil?
        res = client.request(:get, page)
        client.spinner("tickets (total: #{res['count']})", page.split('=').last)
        tickets += ticket_map(res['tickets'], res)
        page = next_page(res)
      end
      puts ' ...done'
      tickets
    end

    # Shows details about a specific ticket
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param tid [Integer] The Ticket ID to use
    # @return [Hash]
    #
    # @example
    #   ZendeskSupportAPI::Tickets.show(client, 35436)
    #   #=> {
    #   #=>   "id":      35436,
    #   #=>   "subject": "My printer is on fire!",
    #   #=>   ...
    #   #=> }

    def self.show(client, tid)
      url = "tickets/#{tid}.json#{includes}"
      res = client.request(:get, url)
      res['ticket'].map { |t| ticket_object(t, res) }
    end

    # Show details about many tickets
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param tids [Array] An array of ticket IDs
    # @return [Array]

    def self.show_many(client, tids)
      url = "tickets/show_many.json#{includes}&ids=#{tids.join(',')}"
      res = client.request(:get, url)
      res['tickets'].map { |t| ticket_object(t, res) }
    end

    # Create a ticket
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param ticket [Hash] The ticket hash to use for creation
    # @return [Hash|String]
    #
    # @example
    #   ticket = {
    #     subject: "This is a test ticket",
    #     followers: [
    #       { user_email: 'albert@example.com', action: 'put' },
    #       { user_id: 123, action: 'put' }
    #     ],
    #     comment: {
    #       body: "This goes into the test ticket body!"
    #     },
    #     tags: ["free_user", "ignore"]
    #   }
    #   ZendeskSupportAPI::Tickets.create(client, ticket)
    #   #=> {
    #   #=>   "id": 1,
    #   #=>   "subject": "This is a test ticket",
    #   #=>   ...
    #   #=> }

    def self.create(client, ticket)
      res = client.request(:post, 'tickets.json', ticket: ticket)
      return "Creation failed: #{res['details']}" if res['error']

      res
    end

    # Creates many tickets
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param tickets [Array] The array of ticket hashes to use for creation
    # @return [ZendeskSupportAPI::Client.handle_job]

    def self.create_many(client, tickets)
      res = client.request(:post, 'tickets/create_many.json', tickets: tickets)
      client.handle_job(res)
    end

    # Updates a ticket
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param tid [Integer] The ticket ID to update
    # @param ticket [Hash] The ticket hash to use
    # @return [Hash|String]

    def self.update(client, tid, ticket)
      res = client.request(:put, "tickets/#{tid}.json", ticket: ticket)
      return "Update of #{tid} failed: #{res['error']}" if res['error']

      res
    end

    # Updates many tickets
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param tickets [Array] The array of ticket hashes to use for updates
    # @return [ZendeskSupportAPI::Client.handle_job]
    #
    # @example
    #   tickets = [
    #     {
    #       id: 123,
    #       tags: ['ultimate', 'urgent']
    #     },
    #     {
    #       id: 124,
    #       followers: [
    #         { user_id: 123, action: 'delete' }
    #       ]
    #     },
    #     {
    #       id: 125,
    #       subject: 'Staging Instance having issues'
    #     }
    #   ]
    #   ZendeskSupportAPI::Tickets.update_many(client, tickets)

    def self.update_many(client, tickets)
      res = client.request(:put, 'tickets/update_many.json', tickets: tickets)
      client.handle_job(res)
    end

    # Mark a ticket as spam and suspend the user
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param tid [Integer] The ticket ID to use
    # @return [String]

    def self.mark_as_spam(client, tid)
      res = client.request(:put, "tickets/#{tid}/mark_as_spam.json")
      return "Unable to mark ticket as spam: #{res['error']}" if res['error']

      "Ticket #{tid} marked as spam"
    end

    # Mark many tickets as spam and suspend their users
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param tids [Array] The ticket IDs to use
    # @return [ZendeskSupportAPI::Client.handle_job]

    def self.mark_many_as_spam(client, tids)
      url = "tickets/mark_many_as_spam.json?ids=#{tids.join(',')}"
      res = client.request(:put, url)
      client.handle_job(res)
    end

    # Merge Tickets into Target Ticket
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param tid [Integer] The Ticket ID to merge into
    # @param hash [Hash] The Hash to use for the merge (see example)
    # @return [ZendeskSupportAPI::Client.handle_job]
    #
    # @example
    #   merge_hash = {
    #     ids: [112, 113, 114],
    #     source_comment: "Closing in favor of #111",
    #     target_comment: "Merging #112, #113, and #114 into this ticket"
    #   }
    #   ZendeskSupportAPI::Tickets.merge(client, 111, merge_hash)

    def self.merge(client, tid, hash)
      res = client.request(:post, "tickets/#{tid}/merge.json", hash)
      client.handle_job(res)
    end

    # List followers of a ticket
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param tid [Integer] The Ticket ID to use
    # @return [Array]

    def self.list_followers(client, tid)
      res = client.request(:get, "tickets/#{tid}/followers")['users']
      res['users'].each do |user|
        user['organization'] = select_obj(res['organizations'], user[org_id])
      end
      res['users']
    end
  end
end
