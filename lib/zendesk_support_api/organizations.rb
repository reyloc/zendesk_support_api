# frozen_string_literal: true

module ZendeskSupportAPI
  # Organizations class - https://developer.zendesk.com/rest_api/docs/support/organizations
  class Organizations
    # Prints out organizations
    #
    # @return [String]

    def self.orgs
      'organizations'
    end

    # Lists Organizations (first 100)
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @return [Array]
    #
    # @example
    #   ZendeskSupportAPI::Organizations.list(client)
    #   #=> [
    #   #=>   {
    #   #=>     "url": "https://zendesk.com/api/v2/organizations/1.json",
    #   #=>     "id": 1,
    #   #=>     "name": "One Organization",
    #   #=>     ...
    #   #=>   },
    #   #=>   ...
    #   #=>   {
    #   #=>     "url": "https://zendesk.com/api/v2/organizations/100.json",
    #   #=>     "id: 100,
    #   #=>     "name": "Other Organization",
    #   #=>     ...
    #   #=>   }
    #   #=> ]

    def self.list(client)
      client.request(:get, "#{orgs}.json")[orgs]
    end

    # Shows info about an organization
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param oid [Integer] The Organization ID to use
    # @return [Hash]
    #
    # @example
    #   ZendeskSupportAPI::Organizations.show(client, 123)
    #   #=> {
    #   #=>   "url": "https://zendesk.com/api/v2/organizations/1.json",
    #   #=>   "id": 1,
    #   #=>   "name": "One Organization",
    #   #=>   ...
    #   #=> }

    def self.show(client, oid)
      client.request(:get, "#{orgs}/#{oid}.json")['organization']
    end

    # Show several organizations
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param oids [Array] An Array of Organization IDs to show
    # @return [Array]
    #
    # @example
    #   ZendeskSupportAPI::Organizations.show(client, [1,2])
    #   #=> [
    #   #=>   {
    #   #=>     "url": "https://zendesk.com/api/v2/organizations/1.json",
    #   #=>     "id": 1,
    #   #=>     "name": "One Organization",
    #   #=>     ...
    #   #=>   },
    #   #=>   {
    #   #=>     "url": "https://zendesk.com/api/v2/organizations/2.json",
    #   #=>     "id": 2,
    #   #=>     "name": "Two Organization",
    #   #=>     ...
    #   #=>   }
    #   #=> ]

    def self.show_many(client, oids)
      ids = "ids=#{oids.join(',')}"
      client.request(:get, "#{orgs}/show_many.json?#{ids}")[orgs]
    end

    # Creates an organization
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param org [Hash] The organization info to use
    # @return [Hash|String]
    #
    # @example
    #   org = {
    #     name: 'Test Organization',
    #   }
    #   ZendeskSupportAPI::Organizations.create(client, org)
    #   #=> {
    #   #=>   "url": "",
    #   #=>   "id": 123,
    #   #=>   "name": "Test Organization",
    #   #=>   ...
    #   #=> }

    def self.create(client, org)
      res = client.request(:post, "#{orgs}.json", organization: org)
      return "Creation failed: #{res['details']}" if res['error']

      res['organization']
    end

    # Creates many organizations
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param orgs [Array] The organizations to create
    # @return [ZendeskSupportAPI::Client.handle_job]

    def self.create_many(client, orgs)
      url = "#{orgs}/create_many.json"
      res = client.request(:post, url, organizations: orgs)
      client.handle_job(res)
    end

    # Creates or updates an organization
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param org [Hash] The organization info to use
    # @return [Hash|String]
    #
    # @example
    #   org = {
    #     name: 'Test Organization',
    #   }
    #   ZendeskSupportAPI::Organizations.create_or_update(client, org)
    #   #=> {
    #   #=>   "url": "",
    #   #=>   "id": 123,
    #   #=>   "name": "Test Organization",
    #   #=>   ...
    #   #=> }

    def self.create_or_update(client, org)
      url = "#{orgs}/create_or_update.json"
      res = client.request(:post, url, organization: org)
      return "Create/Update failed: #{res['description']}" if res['error']

      res['organization']
    end

    # Updates an organization
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param oid [Integer] The Organization ID to use
    # @param org [Hash] The organization info to use
    # @return [Hash|String]
    #
    # @example
    #   ZendeskSupportAPI::Organizations.update(client, 123, org)
    #   #=> {organization}

    def self.update(client, oid, org)
      res = client.request(:post, "#{orgs}/#{oid}.json", organization: org)
      return "Update of #{uid} failed: #{res['error']}" if res['error']

      res['organization']
    end

    # Updates many organizations
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param orgs [Array] The organizations to update
    # @return [ZendeskSupportAPI::Client.handle_job]

    def self.update_many(client, orgs)
      url = "#{orgs}/update_many.json"
      res = client.request(:put, url, organizations: orgs)
      client.handle_job(res)
    end

    # Deletes an organization
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param oid [Integer] The Organization ID to delete
    # @return [String]
    #
    # @example
    #   ZendeskSupportAPI::Organizations.delete(client, 123)
    #   #=> Organization 123 has been deleted
    #   ZendeskSupportAPI::Organizations.delete(client, 123)
    #   #=> "Deletion of 123 failed: RecordNotFound"

    def self.delete(client, oid)
      res = client.request(:delete, "#{orgs}/#{oid}.json")
      return "Deletion of #{oid} failed: #{res['error']}" if res['error']

      "Organization #{uid} has been deleted"
    end

    # Deletes many organizations
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param ids [Array] The array of Organization IDs to delete
    # @return [ZendeskSupportAPI::Client.handle_job

    def self.bulk_delete(client, ids)
      url = "#{orgs}/destroy_many.json?ids=#{ids.join(',')}"
      res = client.request(:delete, url)
      client.handle_job(res)
    end

    # Searches for orgs by their external_id (first 100)
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param eid [String] The External ID to use
    # @return [Array]
    #
    # @example
    #   ZendeskSupportAPI::Organizations.search_by_external_id(client, 'abc123')
    #   #=> [
    #   #=>   {
    #   #=>     "url": "https://zendesk.com/api/v2/organizations/1.json",
    #   #=>     "id": 1,
    #   #=>     "name": "One Organization",
    #   #=>     ...
    #   #=>   },
    #   #=>   ...
    #   #=>   {
    #   #=>     "url": "https://zendesk.com/api/v2/organizations/100.json",
    #   #=>     "id: 100,
    #   #=>     "name": "Other Organization",
    #   #=>     ...
    #   #=>   }
    #   #=> ]

    def self.search_by_external_id(client, eid)
      client.request(:get, "#{orgs}/search.json?external_id=#{eid}")[orgs]
    end

    # Get an Organization's members
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param oid [Integer] The Organization ID to use
    # @return [Array]
    #
    # @example
    #   ZendeskSupportAPI::Organizations.members(client, 123)
    #   #=> [
    #   #=>   {
    #   #=>     "id": 1,
    #   #=>     "name": "Albert",
    #   #=>     "email": "albert@example.com",
    #   #=>     ...
    #   #=>   },
    #   #=>   ...
    #   #=>   {
    #   #=>     "id": 22,
    #   #=>     "name": "Victor",
    #   #=>     "email": "victor@example.com",
    #   #=>     ...
    #   #=>   }
    #   #=> ]

    def self.members(client, oid)
      url = "#{orgs}/#{oid}/organization_memberships.json?include=users"
      client.request(:get, url)['users']
    end
  end
end
