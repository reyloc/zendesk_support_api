# frozen_string_literal: true

module ZendeskSupportAPI
  # Search class - https://developer.zendesk.com/rest_api/docs/support/search
  class Search
    # Perform a search and list results (first 100)
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param query [String] The query to use
    # @param sort [String] The sorting method to use (defaults to revelance)
    # @param order [String] The order to use (defaults to desc)
    # @return [Array]
    #
    # @example
    #   ZendeskSupportAPI::Search.list(client, 'bob')
    #   #=> [
    #   #=>   {
    #   #=>     "name":        "Bob McBob",
    #   #=>     "created_at":  "2009-05-13T00:07:08Z",
    #   #=>     "updated_at":  "2011-07-22T00:11:12Z",
    #   #=>     "id":          211,
    #   #=>     "result_type": "user"
    #   #=>     "url":         "https://zendesk.com/api/v2/users/211.json"
    #   #=>   },
    #   #=>   {
    #   #=>     "name":        "Bob's Company",
    #   #=>     "created_at":  "2009-08-26T00:07:08Z",
    #   #=>     "updated_at":  "2010-05-13T00:07:08Z",
    #   #=>     "id":          122,
    #   #=>     "result_type": "group"
    #   #=>     "url":         "https://zendesk.com/api/v2/groups/122.json"
    #   #=>   },
    #   #=>   ...
    #   #=> ]

    def self.list(client, query, sort = '', order = 'desc')
      return bad_search("invalid sort - #{sort}") unless sort_valid?(sort)
      return bad_search("invalid order - #{order}") unless order_valid?(order)

      sort_order = sort_order_string(sort, order)
      client.request(:get, "search.json?query=#{query}#{sort_order}")['results']
    end

    # Determines if a given sort string is valid
    #
    # @param sort [String] The sort string to check
    # @return [Boolean]
    #
    # @example
    #   ZendeskSupportAPI::Search.sort_valid? 'updated_at'
    #   #=> true
    #   ZendeskSupportAPI::Search.sort_valid? 'ticket_type'
    #   #=> true
    #   ZendeskSupportAPI::Search.sort_valid? 'random'
    #   #=> false

    def self.sort_valid?(sort)
      return true if sort.empty?

      valid = %w[updated_at created_at priority status ticket_type]
      valid.include? sort
    end

    # Determines if a given order string is valid
    #
    # @param order [String] The order string to check
    # @return [Boolean]
    #
    # @example
    #   ZendeskSupportAPI::Search.order_valid? 'asc'
    #   #=> true
    #   ZendeskSupportAPI::Search.order_valid? 'desc'
    #   #=> true
    #   ZendeskSupportAPI::Search.order_valid? 'random'
    #   #=> false

    def self.order_valid?(order)
      valid = %w[asc desc]
      valid.include? order
    end

    # Prints out an error message
    #
    # @param string [String] The error that occurred
    # @return [String]
    #
    # @example
    #   ZendeskSupportAPI::Search.bad_search('invalid sort - name')
    #   #=> "Search cannot be completed: invalid sort - name"

    def self.bad_search(string)
      "Search cannot be completed: #{string}"
    end

    # Determines the sort and order to use in the query
    #
    # @param sort [String] The sort string to use
    # @param order [String] The order string to use
    # @return [String]
    #
    # @example
    #   ZendeskSupportAPI::Search.sort_order_string('', 'asc')
    #   #=> "&order_by=asc"
    #   ZendeskSupportAPI::Search.sort_order_string('priority', 'desc')
    #   #=> "&sort_by=priority"
    #   ZendeskSupportAPI::Search.sort_order_string('updated_at', 'asc')
    #   #=> "&sort_by=updated_at&order_by=asc"

    def self.sort_order_string(sort, order)
      sort = (sort.empty? ? '' : "&sort_by=#{sort}")
      order = (order == 'desc' ? '' : '&order_by=asc')
      "#{sort}#{order}"
    end
  end
end
