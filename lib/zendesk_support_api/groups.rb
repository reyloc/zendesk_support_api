# frozen_string_literal: true

module ZendeskSupportAPI
  # Groups class - https://developer.zendesk.com/rest_api/docs/support/groups
  class Groups
    # Function to return a string that side-loads users
    #
    # @return [String]

    def self.user_map(groups, users)
      groups.map { |g| group_object(g, users) }
    end

    # Creates a group hash (for mappping the user into the group Hash)
    #
    # @param group [Hash] The group details to use
    # @param users [Array] The Array of users to use
    # @return [Hash]

    def self.group_object(group, users)
      group['user'] = users.select { |u| u['id'] == group['user_id'] }
      group
    end

    # Returns the string of the next_page for pagination
    #
    # @param res [Hash] The Hash containing the response from a request
    # @return [nil|String]
    #
    # @example
    #   ZendeskSupportAPI::Users.next_page(response) #=> nil
    #   ZendeskSupportAPI::Users.next_page(response)
    #   #=> "memberships.json?include=users&page=3

    def self.next_page(res)
      (res['next_page'].nil? ? nil : res['next_page'].split('/').last)
    end

    # Lists groups (first 100)
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @return [Array]
    #
    # @example
    #   ZendeskSupportAPI::Groups.list(client)
    #   #=> [
    #   #=>   {
    #   #=>     "name":       "DJs",
    #   #=>     "created_at": "2009-05-13T00:07:08Z",
    #   #=>     "updated_at": "2011-07-22T00:11:12Z",
    #   #=>     "id":         211
    #   #=>   },
    #   #=>   {
    #   #=>     "name":       "MCs",
    #   #=>     "created_at": "2009-08-26T00:07:08Z",
    #   #=>     "updated_at": "2010-05-13T00:07:08Z",
    #   #=>     "id":         122
    #   #=>   }
    #   #=> ]

    def self.list(client)
      client.request(:get, 'groups.json')['groups']
    end

    # Shows assignable groups (first 100)
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @return [Array]
    #
    # @example
    #   ZendeskSupportAPI::Groups.assignable(client)
    #   #=> [
    #   #=>   {
    #   #=>     "name":       "DJs",
    #   #=>     "created_at": "2009-05-13T00:07:08Z",
    #   #=>     "updated_at": "2011-07-22T00:11:12Z",
    #   #=>     "id":         211
    #   #=>   },
    #   #=>   {
    #   #=>     "name":       "MCs",
    #   #=>     "created_at": "2009-08-26T00:07:08Z",
    #   #=>     "updated_at": "2010-05-13T00:07:08Z",
    #   #=>     "id":         122
    #   #=>   }
    #   #=> ]

    def self.assignable(client)
      client.request(:get, 'assignable.json')['groups']
    end

    # Shows info about a specific group
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param gid [Integer] The Group ID
    # @return [Hash]
    #
    # @example
    #   ZendeskSupportAPI::Groups.show(client, 122)
    #   #=> {
    #   #=>   "name":       "MCs",
    #   #=>   "created_at": "2009-08-26T00:07:08Z",
    #   #=>   "updated_at": "2010-05-13T00:07:08Z",
    #   #=>   "id":         122
    #   #=> }

    def self.show(client, gid)
      client.request(:get, "groups/#{gid}.json")['group']
    end

    # Create a group
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param group [Hash] The group details to use
    # @return [String|Hash]
    #
    # @example
    #   group = {
    #     name: 'Test Group'
    #   }
    #   ZendeskSupportAPI::Groups.create(client, group)
    #   #=> {
    #   #=>   "name": "Test Group",
    #   #=>   "created_at": "2011-04-20T17:49:00Z",
    #   #=>    "updated_at": "2011-04-20T17:49:00Z",
    #   #=>    "id":         123
    #   #=> }

    def self.create(client, group)
      res = client.request(:post, 'groups.json', group: group)
      return "Creation failed: #{res['details']}" if res['error']

      res
    end

    # Updates a group
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param gid [Integer] The group ID to use
    # @param group [Hash] The group details to use
    # @return [String|Hash]
    #
    # @example
    #   group = {
    #     name: 'Test Group - defunct'
    #   }
    #   ZendeskSupportAPI::Groups.update(client, 123, group)
    #   #=> {
    #   #=>   "name": "Test Group - defunct",
    #   #=>   "created_at": "2011-04-20T17:49:00Z",
    #   #=>    "updated_at": "2011-07-20T17:49:00Z",
    #   #=>    "id":         123
    #   #=> }

    def self.update(client, gid, group)
      res = client.request(:post, "groups/#{gid}.json", group: group)
      return "Update failed: #{res['details']}" if res['error']

      res
    end

    # Deletes a group
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param gid [Integer] The group ID to use
    # @return [String]
    #
    # @example
    #   ZendeskSupportAPI::Groups.delete(client, 123)
    #   #=> Group 123 has been deleted

    def self.delete(client, gid)
      res = client.request(:delete, "groups/#{gid}.json")
      return "Deletion of #{gid} failed: #{res['error']}" if res['error']

      "Group #{gid} has been deleted"
    end
  end
end
