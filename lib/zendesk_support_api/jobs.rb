# frozen_string_literal: true

module ZendeskSupportAPI
  # Jobs class - https://developer.zendesk.com/rest_api/docs/support/job_statuses
  class Jobs
    # Make a request to show the job status
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param id [String] The id of the job to check
    # @return [Hash]
    #
    # @example
    #   client = ZendeskSupportAPI::Client.new('user', '123', 'zendesk.com/api')
    #   ZendeskSupportAPI::Jobs.show(client, 'abc123')
    #   #=> {
    #   #=>   "job_status"=> {
    #   #=>     "id"=>"abc123",
    #   #=>     "url"=>"abc123.json",
    #   #=>     "total"=>2,
    #   #=>     "progress"=>2,
    #   #=>     "status"=>"completed",
    #   #=>     "message"=>"Completed at 2020-04-29 13:26:40 +0000",
    #   #=>     "results"=> [
    #   #=>       {
    #   #=>         "id"=>369731992959,
    #   #=>         "status"=>"Updated",
    #   #=>         "email"=>"test@example.com"
    #   #=>       },
    #   #=>       {
    #   #=>         "id"=>369728778639,
    #   #=>         "status"=>"Updated",
    #   #=>         "email"=>"test2@example.com"
    #   #=>       }
    #   #=>     ]
    #   #=>   }
    #   #=> }

    def self.show(client, id)
      client.request(:get, "job_statuses/#{id}.json")
    end

    # Make a request to show all job statuses
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @return [Hash]
    #
    # @example
    #   ZendeskSupportAPI::Jobs.show_many(client, ['abc123', 'def456'])
    #   #=> {
    #   #=>   "job_statuses": [
    #   #=>     {
    #   #=>       "id": "abc123",
    #   #=>       "status": "completed",
    #   #=>       ...
    #   #=>     },
    #   #=>     {
    #   #=>      "id": "def456",
    #   #=>      "status": "completed",
    #   #=>      ...
    #   #=>     }
    #   #=>   ]
    #   #=> }

    def self.list(client)
      client.request(:get, 'job_statuses.json')['job_statuses']
    end

    # Show many job statuses
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param ids [Array] An Array of job IDs to show
    # @return [Hash]
    #
    # @example
    #   ZendeskSupportAPI::Jobs.show_many(client, ['abc123', 'def456'])
    #   #=> {
    #   #=>   "job_statuses": [
    #   #=>     {
    #   #=>       "id": "abc123",
    #   #=>       "status": "completed",
    #   #=>       ...
    #   #=>     },
    #   #=>     {
    #   #=>      "id": "def456",
    #   #=>      "status": "completed",
    #   #=>      ...
    #   #=>     }
    #   #=>   ]
    #   #=> }

    def self.show_many(client, ids)
      client.request(:get, "job_statuses/show_many.json?ids=#{ids.join(',')}")
    end
  end
end
