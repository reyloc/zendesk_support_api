# frozen_string_literal: true

module ZendeskSupportAPI
  # Tickets class - https://developer.zendesk.com/rest_api/docs/support/tickets
  class DeletedTickets
    # Determines if given string is a valid order function
    #
    # @param order [String] The order string to use
    # @return [Boolean]
    #
    # @example
    #   ZendeskSupportAPI::DeletedTickets.order_valid? 'desc' #=> true
    #   ZendeskSupportAPI::DeletedTickets.order_valid? 'random' #=> false

    def self.order_valid?(order)
      %w[desc asc].include? order
    end

    # Prints out the sort_by and order_by string for the url
    #
    # @param sort [String] The sort string to use
    # @param order [String] The order string to use
    # @return [String]
    #
    # @example
    #   ZendeskSupportAPI::DeletedTickets.sort_order('id', 'desc')
    #   #=> '&sort_by=id&order_by=desc'

    def self.sort_order(sort, order)
      "&sort_by=#{sort}&order_by=#{order}"
    end

    # Returns the string of the next_page for pagination
    #
    # @param res [Hash] The Hash containing the response from a request
    # @return [nil|String]

    def self.next_page(res)
      (res['next_page'].nil? ? nil : res['next_page'].split('/').last)
    end

    # Prints out organization_id
    #
    # @return [String]
    #
    # @example
    #   ZendeskSupportAPI::DeletedTickets.org_id #=> 'organization_id'

    def self.org_id
      'organization_id'
    end

    # Selects an object from an array based on a user ID
    #
    # @param array [Array] An array of to look in
    # @param id [Integer] The ID to use
    # @return [Hash]

    def self.select_obj(array, id)
      array.select { |a| a['id'] == id }.first
    end

    # Determines if sort value is valid for delete requests
    #
    # @param sort [String] The string to check
    # @return [Boolean]

    def self.deleted_sort_valid?(sort)
      %w[id subject deleted_at].includes?(sort)
    end

    # Lists deleted tickets (first 100)
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param sort [String] The sort string to use
    # @param order [String] The order string to use
    # @return [Array]

    def self.list_deleted(client, sort = 'id', order = 'asc')
      return "Invalid sort '#{sort}'" unless delete_sort_valid?(sort)
      return "Invalid order '#{order}'" unless order_valid?(order)

      url = "deleted_tickets.json?#{sort_order(sort, order)}"
      client.request(:get, url)['deleted_tickets']
    end

    # Lists all deleted tickets
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @return [Array]

    def self.list_all_deleted(client)
      tickets = []
      page = 'deleted_tickets.json?page=1'
      until page.nil?
        res = client.request(:get, page)
        client.spinner("tickets (total: #{res['count']})", page.split('=').last)
        tickets += res['tickets']
        page = next_page(res)
      end
      puts ' ...done'
      tickets
    end

    # Restores a deleted ticket
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param tid [Integer] The ticket ID to use
    # @return [nil|String]

    def self.restore(client, tid)
      res = client.request(:put, "deleted_tickets/#{tid}/restore.json")
      return "Restore of #{tid} failed: #{res['error']}" if res['error']

      res
    end

    # Restores several deleted tickets
    #
    # @param client [ZendeskSupportAPI::Client] The client instance to use
    # @param tids [Array] Array of ticket IDs to use
    # @return [ZendeskSupportAPI::Client.handle_job]

    def self.restore_many(client, tids)
      url = "deleted_tickets/restore_many?ids=#{tids.join(',')}"
      res = client.request(:put, url)
      client.handle_job(res)
    end
  end
end
